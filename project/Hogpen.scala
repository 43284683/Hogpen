import sbt._
import sbt.Keys._
import com.typesafe.sbt.SbtAtmos.{Atmos, atmosSettings}

object HogpenBuild extends Build {

  lazy val hogpen = Project(
    id = "hogpen",
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      name := "hogpen",
      organization := "com.gtan",
      version := "0.1-SNAPSHOT",
      scalaVersion := "2.10.2",
      resolvers += "For Proxool" at "http://maven.cloudhopper.com/repos/third-party/",
      libraryDependencies ++= Seq(
        "com.typesafe.akka" %% "akka-actor" % "2.2.1",
        "com.typesafe.akka" %% "akka-testkit" % "2.2.1",
        "com.typesafe.akka" %% "akka-agent" % "2.2.1",
        "org.scalatest" %% "scalatest" % "1.9.1",
        "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
        "com.alibaba" % "druid" % "0.2.26",
        "org.apache.tomcat" % "tomcat-jdbc" % "7.0.42",
        "commons-dbcp" % "commons-dbcp" % "1.4",
        "proxool" % "proxool" % "0.9.1",
        "c3p0" % "c3p0" % "0.9.1.2",
        "com.jolbox" % "bonecp" % "0.8.0-rc1",
        "commons-logging" % "commons-logging" % "1.1.3"
      )
    )
  ).configs(Atmos)
    .settings(atmosSettings: _*)
}

