package com.gtan.hogpen
import akka.actor.{ActorLogging, Actor}
import com.gtan.hogpen.ConnectionFactory.CreateConnection
import java.sql.{DriverManager, Driver}
import java.util.Properties
import akka.event.{LoggingReceive, Logging}
import akka.pattern.ask
import scala.concurrent.ExecutionContext
import com.gtan.hogpen.HogpenPool.Get

object ConnectionFactory {
  case object CreateConnection
  case object Ready_?
}

class ConnectionFactory extends Actor with AkkaConfig with ActorLogging {
  import ConnectionFactory._
  import concurrent.duration._
  import ExecutionContext.Implicits.global

  val url = config.getString("datasource.url")
  val driver = DriverManager.getDriver(url)
  val dataSourceConfig = new java.util.Properties()
  dataSourceConfig.setProperty("username", config.getString("datasource.username"))
  dataSourceConfig.setProperty("password", config.getString("datasource.password"))

  override def receive: Actor.Receive = LoggingReceive {
    case CreateConnection =>
      val conn = driver.connect(url, dataSourceConfig)
      sender ! conn
  }

}
