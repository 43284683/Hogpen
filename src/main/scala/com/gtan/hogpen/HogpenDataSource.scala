package com.gtan.hogpen

import javax.sql.DataSource
import java.util.logging.Logger
import java.io.PrintWriter
import java.sql.Connection
import akka.actor.{Actor, Props, ActorSystem}
import akka.routing.RoundRobinRouter

class HogpenDataSource(val system: ActorSystem) extends DataSource {

  val maxActive = system.settings.config.getInt("datasource.pool.maxActive")

  val poolCount = 2
  val pools = (0 until poolCount) map {i =>
    system.actorOf(Props(classOf[HogpenPool], maxActive/poolCount)/*.withDispatcher("pool-dispatcher")*/, s"Pool-$i")
  }

  val poolRouter = system.actorOf(Props.empty.withRouter(RoundRobinRouter(routees = pools)), "PoolRouter")


  override def getLogWriter: PrintWriter = ???

  override def setLogWriter(out: PrintWriter) {}

  override def setLoginTimeout(seconds: Int) {}

  override def getLoginTimeout: Int = ???

  override def getParentLogger: Logger = ???

  override def unwrap[T](iface: Class[T]): T = {
    throw new RuntimeException("Unimplemented")
  }

  override def isWrapperFor(iface: Class[_]): Boolean = false

  override def getConnection: Connection = new HogpenConnection(poolRouter)

  override def getConnection(username: String, password: String): Connection = ???
}
