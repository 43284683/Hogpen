package com.gtan.hogpen

import akka.actor.Actor

trait AkkaConfig { self: Actor =>
  def config = context.system.settings.config
}
