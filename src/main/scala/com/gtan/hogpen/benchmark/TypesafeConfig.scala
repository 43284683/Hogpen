package com.gtan.hogpen.benchmark
import com.typesafe.config.ConfigFactory

trait TypesafeConfig {
  val config = ConfigFactory.load()
}
