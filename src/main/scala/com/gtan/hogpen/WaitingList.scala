package com.gtan.hogpen

import akka.actor.{ActorRef, Actor, ActorLogging}
import java.sql.Connection

object WaitingList {
  case class Enqueue(requester: ActorRef)
  case class Dequeue(connection: Connection)
}

class WaitingList extends Actor with AkkaConfig with ActorLogging {
  import WaitingList._

  val queue = collection.mutable.Queue.empty[ActorRef]

  def receive = {
    case Enqueue(requester) =>
      queue.enqueue(requester)
    case Dequeue(connection) =>

  }
}
